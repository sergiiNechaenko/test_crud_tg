<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 50; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $firstName = strtolower($faker->firstName($gender));
            $lasName = strtolower($faker->lastName);
            DB::table('users')->insert([
                'first_name' => $firstName,
                'last_name' => $lasName,
                'email' => $firstName . '.' . $lasName . rand(1000, 9999) . '@gmail.com',
                'status' => collect(['active', 'inactive'])->random(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
