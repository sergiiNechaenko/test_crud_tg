<?php

namespace App\Services;

class TelegramBot
{
    /**
     * @param int $chatId
     * @param string $text
     */
    public function sendMessage(int $chatId, string $text): void
    {
        $url = config('telegram.bot.url');
        $token = config('telegram.bot.token');

        $url .= $token . '/sendMessage?';

        $params = [
            'chat_id' => $chatId,
            'text' => $text,
            'parse_mode' => 'HTML'
        ];

        $flag = true;
        foreach ($params as $key => $param) {
            $url .= ($flag) ? '' : '&';
            $url .= $key . '=' . $param;
            $flag = false;
        }

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        curl_exec($curl);
    }
}
