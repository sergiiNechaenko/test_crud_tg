<?php


namespace App\Http\Controllers;

use App\Services\TelegramBot;
use App\User;

class WebhookController extends Controller
{
    public function index($secret)
    {
        $search = request()->message['text'];
        if ($secret !== config('telegram.webhook.mytoken') || !$search) {
            return;
        }
        if ('/start' === $search) {
            $telegramBot = new TelegramBot();
            $telegramBot->sendMessage((int)request()->message['chat']['id'], urlencode('Type search word...'));
            return;
        }

        $users = User::all();
        $usersFilteredCount = 0;
        $lastUserFiltered = null;
        foreach ($users as $user) {
            if (str_contains($user->first_name, $search)
                || str_contains($user->last_name, $search)
                || str_contains($user->email, $search)
                || str_contains($user->status, $search)) {
                $usersFilteredCount++;
                $lastUserFiltered = $user;
            }
        }
        if (!$lastUserFiltered) {
            $messageText = "User not found!\n";
        } else {
            $messageText = "User found!\n";
            $messageText .= "first_name: " . $lastUserFiltered->first_name . "\n";
            $messageText .= "last_name: " . $lastUserFiltered->last_name . "\n";
            $messageText .= "email: " . $lastUserFiltered->email . "\n";
            $messageText .= "status: " . $lastUserFiltered->status . "\n";
            if ($usersFilteredCount > 1) {
                $usersFilteredCount--;
                $messageText .= "and more $usersFilteredCount users...\n";
            }
        }

        $telegramBot = new TelegramBot();
        $telegramBot->sendMessage((int)request()->message['chat']['id'], urlencode($messageText));
    }
}
