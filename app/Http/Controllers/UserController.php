<?php


namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\User;

class UserController extends Controller
{
    public function delete($id)
    {
        $user = User::find($id);
        if ($user && $user->delete()) {
            return redirect()->back()->with('success', "User {$user->first_name} is deleted");
        }

        return redirect()->back()->withErrors(['User not found']);
    }

    public function create()
    {
        return view('create');
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        if ($user->save()) {
            return redirect()->route('home')->with('success', "User {$user->first_name} is created");
        }
        return redirect()->route('home')->withErrors(['User creating error']);
    }

    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('home')->withErrors(['User not found']);
        }
        return view('edit', compact('user'));
    }

    public function update($id, StoreUserRequest $request)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('home')->withErrors(['User not found']);
        }
        $user->fill($request->all());
        if ($user->save()) {
            return redirect()->route('home')->with('success', "User {$user->first_name} is updated");
        }
        return redirect()->route('home')->withErrors(['User updating error']);
    }
}
