<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/vue-select/3.10.3/vue-select.css">
    <style>
        .pointer {
            cursor: pointer
        }

        .modal-dialog {
            margin-top: 20px
        }
    </style>
    <title>Main Page</title>
</head>
<body>
<div class="row mt-5" id="app">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-2">
                        Users List
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('users.create')}}" class="btn btn-primary btn-sm">
                            Create User <i class="fa fa-plus"></i>
                        </a>
                    </div>

                    <div class="col-md-8">
                        <span class="pull-right text-danger">
                            @if($errors->any())
                                {{$errors->first()}}
                            @endif
                        </span>
                        <span class="pull-right text-success">
                            @if(session()->has('success'))
                                {{ session()->get('success') }}
                            @endif
                        </span>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th><b>id</b></th>
                            <th><b>first_name</b></th>
                            <th><b>last_name</b></th>
                            <th><b>email</b></th>
                            <th><b>status</b></th>
                            <th><b>created_at</b></th>
                            <th><b>updated_at</b></th>
                            <th><b>Actions</b></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->status}}</td>
                                <td>{{$user->created_at}}</td>
                                <td>{{$user->updated_at}}</td>
                                <td class="text-center pointer">
                                    <a href="{{route('users.edit',['id' => $user->id])}}">
                                        <i class="fa fa-edit" title="Edit"></i>
                                    </a>

                                    <a href="{{route('users.delete',['id' => $user->id])}}">
                                        <i class="fa fa-trash" title="Delete"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>

</body>
</html>
