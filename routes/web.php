<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', function () {
    return redirect('/users');
});

Route::get('/users', [\App\Http\Controllers\HomeController::class, 'index'])
    ->name('home');

Route::get('/users/create', [\App\Http\Controllers\UserController::class, 'create'])
    ->name('users.create');

Route::post('/users/store', [\App\Http\Controllers\UserController::class, 'store'])
    ->name('users.store');

Route::get('/users/edit/{id}', [\App\Http\Controllers\UserController::class, 'edit'])
    ->name('users.edit');

Route::post('/users/update/{id}', [\App\Http\Controllers\UserController::class, 'update'])
    ->name('users.update');

Route::get('users/delete/{id}', [\App\Http\Controllers\UserController::class, 'delete'])
    ->name('users.delete');

