
## Test task

- Создать CRUD  пользователей: platform_id, phone, first_name, last_name, email, status, updated_at, created_at
- Создать Telegram Bot с функционалом: поиск пользователей по всем полям
- Фреймворк Yii2 желательно, но не обязательно.
- Залить все на Git
- Скинуть ссылку на бот обратно на почту

## Test task
1. [Ссылка на реализованный CRUD](https://servicedomen.fun/users)
2. [Ссылка на телеграмм бот](https://t.me/test_crud_tg_bot)
